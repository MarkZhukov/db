﻿CREATE temp TABLE IF NOT EXISTS fibonacci(
num BIGINT UNIQUE NOT NULL,
val BIGINT UNIQUE NOT NULL
);

CREATE OR REPLACE FUNCTION fibon(i BIGINT) RETURN IDENTIFIED AS $$
DECLARE
	f BIGINT;
BEGIN
	IF i = 0 THEN
		RETURN 0;
	END IF;
	IF i = 1 THEN
		RETURN 1;
	END IF;
	SELECT val INTO f FROM fibonacci WHERE num = i;
	IF f IS NOT NULL THEN
		RETURN f;
	END IF;
	f := fibon(i-1) + fibon(i-2);
	INSERT INTO fibonacci VALUES(i, f);
	RETURN f;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM fibon(10); -- nth fibonacci number
SELECT * FROM fibonacci;