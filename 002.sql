sql

CREATE TABLE department (
id int PRIMARY KEY,
name varchar(20)
)


SELECT department FROM employ


INSERT INTO department (id,name)
values(1,'lime'), (2,'NEMAGIA') ,(3,'guys')


alter table employ ADD CONSTRAINT department_id_fk FOREIGN KEY 
(department_id) references department(id)


SELECT department.name FROM employ JOIN department 
ON employ.department_id=department.id GROUP BY 
department.name HAVING count(employ.id) <= 4


SELECT m_name, max(l_salary) FROM 
(SELECT department.name AS m_name, sum(salary) AS l_salary FROM
employ join department on employ.department_id = department.id 
GROUP BY m_name HAVING avg(l_salary) = (SELECT max(l_salary) FROM
(SELECT department.name AS m_name, sum(salary) AS l_salary FROM
employ join department on employ.department_id = department.id
GROUP BY department.name) as res))


SELECT frst.name frst.department_id from employ as sec 
join employ as frst on sec.id=frst.manager_id
where sec.department_id != frst.department_id 


