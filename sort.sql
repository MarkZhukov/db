﻿CREATE OR REPLACE FUNCTION sort(arr INTEGER[]) RETURNS INTEGER[] AS $$
DECLARE 
	x INTEGER;
	y INTEGER;
	z INTEGER;
BEGIN
	FOR x IN 1..array_length(arr, 1) LOOP
		FOR y IN 1..(array_length(arr, 1) - x) LOOP
			IF arr[y] > arr[y+1] THAN
				z := arr[y];
				arr[y] := arr[y+1];
				arr[y+1] := z;
			END IF;
		END LOOP;
	END LOOP;
	RETURN arr;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM sort(ARRAY[2,3,1,5,-1,8,4,-6]);