--create
CREATE TABLE "Artists" (
	"id" serial NOT NULL,
	"name" TEXT NOT NULL,
	"info_url" TEXT NOT NULL,
	"main_photo" integer UNIQUE,
	CONSTRAINT Artists_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "News" (
	"id" serial NOT NULL,
	"title" TEXT NOT NULL UNIQUE,
	"content_url" TEXT NOT NULL,
	"date" DATE NOT NULL,
	"main_photo" integer UNIQUE,
	CONSTRAINT News_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "News_photos" (
	"id" serial NOT NULL,
	"news_id" integer,
	"photo_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT News_photos_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Artist_photo" (
	"id" serial NOT NULL,
	"artist_id" integer,
	"content_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT Artist_photo_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);

CREATE TABLE "Artist_video" (
	"id" serial NOT NULL,
	"artist_id" integer,
	"content_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT Artist_video_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);




CREATE TABLE "Users" (
	"id" serial NOT NULL,
	"username" TEXT NOT NULL UNIQUE,
	"name" TEXT NOT NULL,
	"password" TEXT NOT NULL,
	"city" TEXT,
	"about" TEXT,
	"img_url" TEXT,
	CONSTRAINT Users_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "User_links" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL,
	"link_url" TEXT NOT NULL UNIQUE,
	CONSTRAINT User_links_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Admins" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL UNIQUE,
	CONSTRAINT Admins_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Topicks" (
	"id" serial NOT NULL,
	"title" TEXT NOT NULL,
	"initial_comment" integer UNIQUE,
	CONSTRAINT Topicks_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Comments" (
	"id" serial NOT NULL,
	"user_id" integer NOT NULL,
	"content" TEXT NOT NULL,
	"date_time" TIMESTAMP,
	"topick_id" integer,
	CONSTRAINT Comments_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Tags" (
	"id" serial NOT NULL,
	"tag" TEXT NOT NULL UNIQUE,
	CONSTRAINT Tags_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "Tags_in_topicks" (
	"id" serial NOT NULL,
	"tag_id" integer NOT NULL,
	"topick_id" integer NOT NULL,
	CONSTRAINT Tags_in_topicks_pk PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);
-----------------------------------------------------------------------
--const
ALTER TABLE "Artists" ADD CONSTRAINT "Artists_fk0" FOREIGN KEY ("main_photo") REFERENCES "Artist_photo"("id");

ALTER TABLE "News" ADD CONSTRAINT "News_fk0" FOREIGN KEY ("main_photo") REFERENCES "News_photos"("id");

ALTER TABLE "News_photos" ADD CONSTRAINT "News_photos_fk0" FOREIGN KEY ("news_id") REFERENCES "News"("id");

ALTER TABLE "Artist_photo" ADD CONSTRAINT "Artist_photo_fk0" FOREIGN KEY ("artist_id") REFERENCES "Artists"("id");

ALTER TABLE "Artist_video" ADD CONSTRAINT "Artist_video_fk0" FOREIGN KEY ("artist_id") REFERENCES "Artists"("id");

ALTER TABLE "User_links" ADD CONSTRAINT "User_links_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");

ALTER TABLE "Admins" ADD CONSTRAINT "Admins_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");

ALTER TABLE "Topicks" ADD CONSTRAINT "Topicks_fk0" FOREIGN KEY ("initial_comment") REFERENCES "Comments"("id");

ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk0" FOREIGN KEY ("user_id") REFERENCES "Users"("id");
ALTER TABLE "Comments" ADD CONSTRAINT "Comments_fk1" FOREIGN KEY ("topick_id") REFERENCES "Topicks"("id");


ALTER TABLE "Tags_in_topicks" ADD CONSTRAINT "Tags_in_topicks_fk0" FOREIGN KEY ("tag_id") REFERENCES "Tags"("id");
ALTER TABLE "Tags_in_topicks" ADD CONSTRAINT "Tags_in_topicks_fk1" FOREIGN KEY ("topick_id") REFERENCES "Topicks"("id");

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
--inserts
INSERT INTO "Users" (id,username,name,password,city,about,img_url)
values(1,'bad','evgen','comedian','moscow','hate jokes','../../img/bad.jpg');

INSERT INTO "Admins" (user_id)
values(1);

INSERT INTO "Artists" (name,info_url)
values('[BadComedian]','hate, jokes, irony');

INSERT INTO "Artist_photo" (artist_id,content_url)
values(1,'../../img/bad1.jpg');

UPDATE "Artists" SET main_photo=1 WHERE id=1;
------
INSERT INTO "Users" (id ,username,name,password,city,about,img_url)
values(9,'xena','xenady','passwordG','cityG','aboutG','../../img/imgG.jpg');

INSERT INTO "Artists" (id,name,info_url)
values(5,'art2','about2');

INSERT INTO "Artist_photo" (artist_id,content_url)
values(5,'../../img/art2.jpg');
----------------------------------
INSERT INTO "Topicks" (id,title)
values(2,'who wanna drop table?');

INSERT INTO "Topicks" (id,title)
values(1'who admin?');
-----------------------------------
INSERT INTO "Comments" (content,user_id,topick_id)
VALUES('me',1,1);

INSERT INTO "Comments" (content,user_id,topick_id)
VALUES('lol',8,2)

INSERT INTO "Comments" (content,user_id,topick_id)
VALUES('kek',9,2);

INSERT INTO "Comments" (content,user_id,topick_id)
VALUES('me',1,2);
-------------------------------------
INSERT INTO "Tags" (id,tag)
VALUES(9,'admin');

INSERT INTO "Tags" (id,tag)
VALUES(5,'table');
-------------
INSERT INTO "Tags_in_topicks" (tag_id,topick_id)
VALUES(9,1);

INSERT INTO "Tags_in_topicks" (tag_id,topick_id)
VALUES(5,2);
-----------------------------------------------------------------------------------
--index
CREATE INDEX ON "Users"(id);
CREATE INDEX ON "Artist_photo"(id);
CREATE INDEX ON "Artists"(id);
CREATE INDEX ON "Tags"(id);
CREATE INDEX ON "Topicks"(id);
CREATE INDEX ON "Comments"(topick_id);
----------------------------------------------------------------------------------
--targget
CREATE OR REPLACE FUNCTION process_user_edit() RETURNS TRIGGER AS $user_edit$
BEGIN
IF (TG_OP = 'DELETE') THEN
INSERT INTO user_edit SELECT 'D', now(), user, OLD.*;
RETURN OLD;
ELSIF (TG_OP = 'UPDATE') THEN
INSERT INTO user_edit SELECT 'U', now(), user, NEW.*;
RETURN NEW;
ELSIF (TG_OP = 'INSERT') THEN
INSERT INTO user_edit SELECT 'I', now(), user, NEW.*;
RETURN NEW;
END IF;
RETURN NULL; 
END;
$user_edit$ LANGUAGE plpgsql;

CREATE TRIGGER user_edit
AFTER INSERT OR UPDATE OR DELETE ON "Users"
FOR EACH ROW EXECUTE PROCEDURE process_user_edit();
insert into "Users" (username,password) values
('usname', 'password');
delete from "Users" where id=13;
create table "User"(
id serial not null primary key,
login varchar(20) not null unique,
password text not null
)
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--selects
Select * from 
(select * from tags 
join "Tags_in_topicks" as tt 
on "Tags".id = tt.tag_id) as f 
join "Topicks" 
on f.tag_id = "Topicks".tag_id 
where f.name = 'teg';

select * from "Artists" WHERE id = 'art_id';

SELECT * FROM "Comments" WHERE topick_id= 'topic_id';

select content_url from "Artist_photo" WHERE artist_id='id';
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--view
--1 the most popular tours
CREATE VIEW tag_the_pop AS
  SELECT
    t.id           AS tour_id,
    city_from.name AS city_from,
    city_to.name   AS city_to,
    count(*)       AS cnt
  FROM Tags AS t
    JOIN Tags_in_topicks AS tt ON tt.tag_id = t.id
    JOIN Tags AS t ON t.id = c.tour_id
    JOIN Topicks AS top ON top.id = tt.topick_id
  GROUP BY t.id, t.tag
  ORDER BY cnt DESC;
SELECT *
FROM tag_the_pop;





